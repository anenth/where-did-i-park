package com.zen.parking;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zen.parking.util.Constants;

import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MapActivity extends FragmentActivity implements
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener {

	private GoogleMap parkingMap;
	private SharedPreferences appPref;
	private LocationClient mLocationClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		appPref = (SharedPreferences) getSharedPreferences(Constants.APP_DB_NAME, MODE_PRIVATE);
		mLocationClient = new LocationClient(this, this, this);
		parkingMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		parkingMap.setMyLocationEnabled(true);

		if (appPref.getString(Constants.APP_DB_FIELD_PARKING_PLOT_LATITUDE, null) != null) {
			parkingMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(Double.parseDouble(appPref.getString(Constants.APP_DB_FIELD_PARKING_PLOT_LATITUDE, null)), Double.parseDouble(appPref.getString(
							Constants.APP_DB_FIELD_PARKING_PLOT_LONGITUDE, null))), 15.0f));

			parkingMap.addMarker(new MarkerOptions()
					.position(
							new LatLng(Double.parseDouble(appPref.getString(Constants.APP_DB_FIELD_PARKING_PLOT_LATITUDE, null)), Double.parseDouble(appPref.getString(
									Constants.APP_DB_FIELD_PARKING_PLOT_LONGITUDE, null)))).title("My Spot")
					.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

		} else {
			parkingMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(37.786301, -122.417686), 15.0f));
			
			Toast.makeText(getApplicationContext(), "Parking spot not set yet: Choose your office location and hit Save button.", Toast.LENGTH_LONG).show();
			Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
			startActivity(i);
			
			finish();//exit the current activity
		}
		
		parkingMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {			
			@Override
			public void onInfoWindowClick(Marker marker) {
				
				
				int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
				// If Google Play services is available
				if (ConnectionResult.SUCCESS == resultCode) {
					// Get the current location
					Location currentLocation = mLocationClient.getLastLocation();
					//Toast.makeText(getApplicationContext(), currentLocation.getLatitude()+","+currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();	
					Intent intent = new Intent(Intent.ACTION_VIEW, 
						    Uri.parse("http://maps.google.com/maps?saddr=" + currentLocation.getLatitude() + "," + currentLocation.getLongitude() +
						    "&daddr=" + appPref.getString(Constants.APP_DB_FIELD_PARKING_PLOT_LATITUDE, "") + "," + appPref.getString(Constants.APP_DB_FIELD_PARKING_PLOT_LONGITUDE, "") + "&dirflg=w"));
						intent.setComponent(new ComponentName("com.google.android.apps.maps", 
						    "com.google.android.maps.MapsActivity"));
						startActivity(intent);
				}else{
					Toast.makeText(getApplicationContext(), "Sorry, I am unable to find your current location.", Toast.LENGTH_SHORT).show();	
				}
			}
		});
		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:

			Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
			startActivity(settingsIntent);

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onStart() {

		super.onStart();

		/*
		 * Connect the client. Don't re-start any requests here; instead, wait
		 * for onResume()
		 */
		mLocationClient.connect();

	}

	@Override
	protected void onStop() {
		// After disconnect() is called, the client is considered "dead".
		mLocationClient.disconnect();
		super.onStop();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}

}
