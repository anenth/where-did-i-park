package com.zen.parking;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.zen.parking.util.Constants;
import android.location.Location;
import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ParkingActivity extends FragmentActivity implements GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

	private Button parkedButton;
	private LocationClient locationClient;
	private Location currentLocation = null;
	private SharedPreferences appPref;
	private SharedPreferences.Editor appPrefEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parking);

		appPref = (SharedPreferences) getSharedPreferences(Constants.APP_DB_NAME, MODE_PRIVATE);

		locationClient = new LocationClient(this, this, this);

		parkedButton = (Button) findViewById(R.id.parkedButton);
		parkedButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				currentLocation = locationClient.getLastLocation();
				if (currentLocation != null) {
					Toast.makeText(getApplicationContext(), "Saving the plot:" + currentLocation.getLatitude() + "," + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();

					appPrefEditor = appPref.edit();
					appPrefEditor.putString(Constants.APP_DB_FIELD_PARKING_PLOT_LATITUDE, "" + currentLocation.getLatitude());
					appPrefEditor.putString(Constants.APP_DB_FIELD_PARKING_PLOT_LONGITUDE, "" + currentLocation.getLongitude());
					appPrefEditor.apply();

					finish();
				}
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.parking, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_settings:

			Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
			startActivity(settingsIntent);

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		// Connect the client.
		locationClient.connect();
	}

	/*
	 * Called when the Activity is no longer visible.
	 */
	@Override
	protected void onStop() {
		// Disconnecting the client invalidates it.
		locationClient.disconnect();
		super.onStop();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Toast.makeText(getApplicationContext(), "Location Client Connection Failed", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		// Toast.makeText(getApplicationContext(), "Location Client Connected" ,
		// Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDisconnected() {
		// Toast.makeText(getApplicationContext(),
		// "Location Client Disconnected" , Toast.LENGTH_SHORT).show();
	}
}
