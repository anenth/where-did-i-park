package com.zen.parking.util;

public class Constants {

	public static final String APP_TAG= "GPS_TIMER";
	public static final String APP_DB_NAME= "GPS_TIMER";
	public static final String APP_DB_FIELD_SECONDS= "APP_DB_SECONDS";	  
	public static final String APP_DB_FIELD_RESET_STOP= "APP_DB_RESET_STOP";	  
	public static final String APP_DB_FIELD_TIMER_IN_PROGRESS= "APP_DB_TIMER_IN_PROGRESS";	  

	public static final String APP_DB_FIELD_PARKING_PLOT_LATITUDE= "APP_DB_FIELD_PARKING_PLOT_LATITUDE";	  
	public static final String APP_DB_FIELD_PARKING_PLOT_LONGITUDE= "APP_DB_FIELD_PARKING_PLOT_LONGITUDE";	  
}
