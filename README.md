# Where Did I Park? - ReadMe

The app assists users in finding the exact parking spot during commute. The user needs to specify the office location (on first use). When the user enters the Geofence (300m radius) in the morning, the user will be prompted to specify a parking spot. The user, opens the notification and sets the exact spot after getting out of his car.

Later in the evening, when the app is opened, the exact parking spot is shown on a map along with the user's location. User can opt to get walking direction to that spot.


#Note

-    The app uses Google Play Services SDK and it is built on top to Geofence Sample App provided in the online documentation.
-	The step by step screenshots of the app usage is provided in the Screenshots folder.
-	To test the app, mock locations can be provided using this app (https://play.google.com/store/apps/details?id=org.ajeje.fakelocation). 
